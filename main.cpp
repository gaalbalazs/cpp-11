//
//  main.cpp
//  movecostructor_test
//
//  Created by Gaál Balázs on 2018. 04. 17..
//  Copyright © 2018. ConSteel. All rights reserved.
//

#include <iostream>
#include <vector>
#include <algorithm>
#include <memory>
#include "Classes.h"

using namespace std;

void f(A a)
{
    
}

A g(A a)
{
    return a;
}

int main(int argc, const char * argv[]) {
    
    vector<unique_ptr<A>> v;
    v.push_back(unique_ptr<A>(new A(1)));
    v.push_back(unique_ptr<A>(new A(2)));
    v.push_back(unique_ptr<A>(new A(3)));

    A a(50);
    A b( g(a) );
    
    int n=10;
    for_each(v.begin(), v.end(), mem_fn(&A::MemberFunction));
    for_each(v.begin(), v.end(), bind(mem_fn(&A::MemberFunction), &b));
    for_each(v.begin(), v.end(), bind(mem_fn(&A::MemberFunctionInt), placeholders::_1, n));
    
    return 0;
}
