//
//  Classes.h
//  movecostructor_test
//
//  Created by Gaál Balázs on 2018. 04. 17..
//  Copyright © 2018. ConSteel. All rights reserved.
//

#ifndef Classes_h
#define Classes_h

class A
{
    int* m_pData= nullptr;
    
public:
    A();
    A(int i);
    A(const A& v);
    A(A&& v);
    
    virtual ~A();
    
    A& operator=(const A& v);
    
    void MemberFunction();
    void MemberFunctionInt(const int& i);
};

#endif /* Classes_h */
