//
//  Classes.cpp
//  movecostructor_test
//
//  Created by Gaál Balázs on 2018. 04. 17..
//  Copyright © 2018. ConSteel. All rights reserved.
//

#include "Classes.h"
#include <iostream>
#include <algorithm>

using namespace std;

A::A() : m_pData(new int(42))
{
    cout << "Constuctor" << endl;
}

A::A(int i) : m_pData(new int(i))
{
    cout << "Constuctor(int)" << endl;
}

A::A(const A& v) : m_pData(new int(0))
{
    *m_pData= *v.m_pData;
    cout << "Copy constuctor" << endl;
}

A::A(A&& v)
{
    swap( m_pData, v.m_pData );
    cout << "Move constuctor" << endl;
}

A& A::operator=(const A& v)
{
    cout << "= operator" << endl;
    *m_pData= *v.m_pData;
    return *this;
}

A::~A()
{
    if( m_pData )
        delete m_pData;
    
    cout << "Destuctor" << endl;
}

void A::MemberFunction()
{
    cout << "MemberFunction " << *m_pData << endl;
}

void A::MemberFunctionInt(const int& i)
{
    cout << "MemberFunction " << *m_pData + i << endl;
}

